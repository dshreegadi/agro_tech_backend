const Sequelize = require('sequelize');
const db = require('../config/global-keys');


module.exports = new Sequelize(db.dbConfig.dbName, db.dbConfig.dbUser, db.dbConfig.dbPass, {
    host: db.dbConfig.host,
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});
