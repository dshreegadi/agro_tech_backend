const jwt = require('jsonwebtoken');
const User = require('../models/otp');


module.exports = {
    auth: async (req, res, next) => {
        const token = req.header('Authorization');
        // check for token
        if (!token) res.status(401).json({ message: 'Unauthorized user' });
        try {
            // verify token
            const decoded = jwt.verify(token, 'secretKey');
            // add user from payload
            // attributes: { exclude: ['otp', 'isOtpVerified', 'password']
            User.findOne({
                where: {
                    id: decoded['id']
                },
                attributes: {
                    exclude: ['password', 'createdAt', 'updatedAt', 'isOtpVerified', 'otp']
                }
            }).then(found => {
                let { dataValues } = found;
                let x = { id, name, username } = dataValues;
                req.user = x;
            }).catch((err) => {
                console.log(err);
            })
            next();
        } catch (e) {
            res.status(400).json({ message: 'Token is not valid' })
        }
    }
}
