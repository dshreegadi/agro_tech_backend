Sequelize = require('sequelize');
Question = require('../models/question');
Answer = require('../models/answer');
Otp = require('../models/otp');

module.exports = {
    addQuestion: async (req, res, next) => {
        let { farmer_id, questionDescription } = req.body;
        console.log(farmer_id);
        console.log(questionDescription);

        Question.create({
            userId: farmer_id,
            // questionId: q_id,
            questionDescription: questionDescription
        }).then((response) => {
            let dataValues = response.dataValues;
            res.status(200).json({
                status: true,
                message: 'Your question is added successfully',
                data: [dataValues]
            })
        }).catch((err) => {
            console.log(err);
            res.status(500).json({
                status: false,
                message: 'Something went wrong',
            })
        })
    },

    getAllQueries: async (req, res, next) => {
        // id will be either actual id OR null
        let { id } = req.body;
        Question.findAll({
            where: !id ? Sequelize.literal('1 = 1') : { userId: id },
            include: [
                { model: Answer, include: [Otp] },
                { model: Otp, attributes: { exclude: ['otp', 'isOtpVerified', 'password'] }, required: false }
            ],
        }).then((question) => {
            res.status(200).json({
                status: true,
                data: question,
                loggeInUser: req.user
            });

        }).catch((error) => {
            res.status(500).json({
                status: false,
                data: error
            });
        });
    }
}