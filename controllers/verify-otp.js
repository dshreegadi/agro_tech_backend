// Users = require('../models/users');
sendOtpModel = require('../models/otp');

module.exports = {
    verifyOtp: async (req, res, err) => {
        let { username, otp } = req.body;
        let errors = [];
        if (!username) {
            errors.push('Please provide username')
        }
        if (!otp) {
            errors.push('Please provide otp')
        }
        if (errors.length > 0) {
            res.status('add', {
                errors,
                username,
                otp
            })
            res.status(500).json({ message: errors })
        } else {
            sendOtpModel.findOne({ where: { username: username } }).then(userName => {
                if (userName) {
                    isNewRecord = userName._options.isNewRecord;
                    if (!isNewRecord) {
                        let { username: $username, otp: $otp } = userName.dataValues;

                        if (username == $username && otp == $otp) {

                            sendOtpModel.update(
                                { isOtpVerified: 'true' },
                                { where: { username: $username } }
                            ).then(success => {
                                res.status(200).json({ message: `Username registered succsessfully` });
                            })
                        } else {
                            if (username != $username) {
                                errors.push('Invalid username')
                            }
                            if (otp != $otp) {
                                errors.push('Invalid otp')
                            }
                            res.status(500).json({ message: errors })
                        }
                    }
                } else {
                    res.status(500).json({ message: `${username} not found` })
                }
            });
        }
    }
}