const Answer = require('../models/answer');

module.exports = {
    addAnswer: async (req, res, err) => {
        let { expert_id, q_id, answerDesc } = req.body;

        Answer.create({
            userId: expert_id,
            questionId: q_id,
            answerDesc: answerDesc
        }).then((response) => {
            let dataValues = response.dataValues;
            res.status(200).send({
                status: true,
                message: 'Answer posted successfully',
                data: [dataValues]
            })
        }).catch((err) => {
            console.log(err);
            res.status(500).send({
                status: false,
                message: 'Something went wrong'
            })
        });
    }
}