const jwt = require('jsonwebtoken');
const auth = require('../helpers/auth');
const Otp = require('../models/otp');


module.exports = {


    login: async (req, res, err) => {
        let { username, password } = req.body;
        console.log(username, password)
        let errors = [];

        if (!username) {
            errors.push('Please provide username')
        }
        if (!password) {
            errors.push('Please provide password')
        }
        if (errors.length > 0) {
            res.status('add', {
                errors,
                username,
                password,
            })
            res.status(500).json({ message: errors })
        } else {
            Otp.findOne({ where: { username: username } }).then(found => {
                let { dataValues } = found;
                let { id } = dataValues;
                if (username === dataValues.username && password === dataValues.password && dataValues.isOtpVerified === 'true') {
                    jwt.sign({ id }, 'secretKey', { expiresIn: '3600s' }, (err, token) => {
                        res.status(200).json({
                            status: true,
                            token: token,
                            message: 'Logged in successfully.',
                            data: dataValues
                        });
                    });
                } else {
                    if (password !== dataValues.password) {
                        res.status(500).json({
                            status: false,
                            message: 'Invalid password'
                        });
                    }
                    if (dataValues.isOtpVerified === 'false') {
                        res.status(500).json({
                            status: false,
                            message: 'Otp not yet verified'
                        });
                    }
                }
            }).catch((err) => {
                res.status(500).json({ message: 'User not found' });
            });
            // Register.findOne({ where: { username: username } }).then(found => {
            //     let { dataValues } = found;
            //     if (username === dataValues.username && password === dataValues.password) {
            //         jwt.sign({ username }, 'secretkey', { expiresIn: '30s' }, (err, token) => {
            //             res.status(200).json({
            //                 status: true,
            //                 token: 'bearer ' + token,
            //                 message: 'Logged in successfully.',
            //                 data: dataValues
            //             });
            //         });
            //     } else {
            //         if (password !== dataValues.password) {
            //             res.status(500).json({
            //                 status: false,
            //                 message: 'Invalid password'
            //             });
            //         }
            //     }
            // }).catch((err) => {
            //     res.status(500).json({ message: 'User not found' });
            // });
        }
    },

    register: async (req, res, err) => {
        let { name, username, password } = req.body;
        let errors = [];
        if (!username) {
            errors.push('Please provide username')
        }
        if (!password) {
            errors.push('Please provide password')
        }
        if (!name) {
            errors.push('Please provide name')
        }
        if (errors.length > 0) {
            res.status('add', {
                errors,
                username,
                password,
                name
            })
            res.status(500).json({ message: errors })
        } else {

            // Register.findOne({ where: { username: username } })
            //     .then(success => {
            //         if (success) {
            //             res.status(500).json({ message: `${username} is already registered` })
            //         } else {
            //             Otp.findOne({ where: { username: username } })
            //                 .then(success => {

            //                     if (success) {
            //                         let { isOtpVerified: $isOtpVerified, regType: $regType } = success.dataValues;
            //                         if ($isOtpVerified === 'true') {
            //                             Register.create({
            //                                 username: username,
            //                                 password: password,
            //                                 name: name,
            //                                 regType: $regType
            //                             }).then((response) => {
            //                                 let payload = {};
            //                                 payload['message'] = `${username} has been registered successfully`;
            //                                 payload['data'] = response.dataValues;
            //                                 res.status(200).json(payload)
            //                             }).catch((err) => {
            //                                 res.status(500).json({ message: `Something went wrong` })
            //                             });
            //                         } else {
            //                             res.status(500).json({ message: 'Otp not yet verified' })
            //                         }
            //                     } else {
            //                         res.status(500).json({ message: 'Otp not yet generated.Please generate otp' })
            //                     }
            //                 })
            //                 .catch(err => {
            //                     res.status(500).json({ message: 'Otp not yet generated.Please generate otp' })
            //                 })
            //         }
            //     })
        }
    }
}