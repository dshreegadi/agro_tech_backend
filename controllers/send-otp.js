notifyHelper = require('../helpers/notify');
SendOtp = require('../models/otp');
const bcrypt = require('bcrypt');
const auth = require('../helpers/auth');


module.exports = {
    sendOtp: async (req, res, err) => {
        const otp = Math.floor(Math.random() * 899999 + 100000);
        let { name, username, password, regType } = req.body;

        console.log(name);
        console.log(username);
        console.log(password);
        console.log(password);

        let errors = [];
        if (!username) {
            errors.push('Please provide username')
        }
        if (!password) {
            errors.push('Password cannot be empty')
        }
        if (!regType) {
            errors.push('Please provide the type of registration')
        }
        if (!name) {
            errors.push('Please provide name')
        }
        if (errors.length > 0) {
            // res.status('add', {
            //     errors,
            //     username,
            //     name,
            //     password,
            //     regType
            // })
            res.status(500).json({ message: errors })
        } else {

            let hashedPassword;
            await bcrypt.hash(password, 10).then((hash) => {
                hashedPassword = hash;
            });
            // storing this logic in a differenc func
            // const pass = auth.encryptPassword(password);
            // pass.then(function (pass) {
            //     console.log('DONE pass', pass);
            // })
            // end

         
            // Check if password matcheds
            var result = bcrypt.compareSync('qwesrty', hashedPassword);
            if (result) {
                // password match
            } else {
                //  incorrect password
            }

            SendOtp.findOne({ where: { username: username } }).then(found => {
                if (found === null) {
                    SendOtp.create({
                        username: username,
                        otp: otp,
                        regType: regType,
                        name: name,
                        password: hashedPassword,
                        isOtpVerified: 'false'
                    }).then(response => {
                        notifyHelper.generateOTP({ username: username, otp: otp });
                        res.status(200).json({ message: `Otp has been sent on ${username}` })
                    }).catch(err => console.log('Caught', err));
                } else {
                    let { username, name, password, otp } = found;
                    res.status(500).json({ message: `${username} already exists` });
                }
            }).catch((err) => {
                console.log(err);
            });
        }
    }
}