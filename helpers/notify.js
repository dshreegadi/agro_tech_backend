const nodemailer = require('nodemailer');
const mail = require('../config/global-keys');

module.exports = {

    generateOTP: async function (data) {
        let { username, otp } = data;

        let transporter = nodemailer.createTransport({
            service: 'gmail',
            secure: true,
            auth: {
                user: mail.mailCreds.emailId,
                pass: mail.mailCreds.password
            }
        });

        var mailOptions = {
            from: mail.mailCreds.senderAddress,
            to: `${username}`, // list of receivers
            subject: 'Agro tech',
            // text: `Please enter the following otp and : `,
            html: `<p>Please enter the following otp: <h2> ${otp} </h2>`
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log("***********" + error);
            } else {
                console.log('*********Email sent:********' + info.response);
            }
        });
    }
}
