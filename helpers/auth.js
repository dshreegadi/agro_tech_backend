
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = {

    verifyToken: async (req, res, next) => {
        const authorizationHeaader = req.headers.authorization;
        let result;
        if (authorizationHeaader) {
            try {
                const bearerToken = req.headers.authorization.split(' ')[1]; // Bearer <token>
                req.token = bearerToken;
                next();
            } catch (err) {
                // Throw an error just in case anything goes wrong with verification
                throw new Error(err);
            }
        } else {
            result = {
                error: `Authentication error. Token required.`,
                status: 401
            };
            res.status(401).send(result);
        }
    },

    encryptPassword: async function (password) {
        try {
            const hash = await bcrypt.hash(password, 10).then((hash) => {
                return hash;
            });
            return hash;
        } catch (error) {
            next(error)
        }
    },

    // signToken: async (username) => {
    //     jwt.sign({ username }, 'secretkey', { expiresIn: '30s' }, (err, token) => {
    //         // res.json(token)
    //         res.status(200).json({
    //             status: true,
    //             token: 'bearer ' + token
    //         });
    //     })
    // }
}