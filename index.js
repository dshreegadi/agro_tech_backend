
// Import external modules
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
var db = require('./config/database');

// Initialize middleware
const app = express();
// Enable CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// Test DB

// if force set to true drops all data from all the tables
db.sync({
    // logging: console.log,
    force: false
}).catch('CAUGHT')

db.authenticate()
    .then(() => console.log('Database connected...'))
    .catch(err => console.log('Error: ' + err))

// Setup static assests directory
app.use('/assets', express.static(path.join(__dirname, 'assets')))

// // Add body-parser 
// app.use(bodyParser.json({ limit: '10mb', extended: true }))
// app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))


app.get('/', function (req, res) {
    res.send("App Working.....");
});


// Api Routes
app.use('/send-otp', require('./routes/otp'));
app.use('/verify-otp', require('./routes/verify-otp'));
// app.use('/register', require('./routes/register'));
app.use('/login', require('./routes/login'));
app.use('/query', require('./routes/query'));
app.use('/question', require('./routes/question'));
app.use('/answer', require('./routes/answer'));




//Start server
const PORT = process.env.PORT || 5000;
app.listen(PORT);
console.log('Agro tech server is running at port :' + PORT)
