const router = require('express-promise-router')();
const answerConroller = require('../controllers/answer')

router.route('/')
    .post(answerConroller.addAnswer);

module.exports = router;