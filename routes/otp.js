const router = require('express-promise-router')();
const sendOtpController = require('../controllers/send-otp')

router.route('/')
    .post(sendOtpController.sendOtp);

module.exports = router;