const router = require('express-promise-router')();
const queryController = require('../controllers/query');

router.route('/')
    .post(queryController.askQuestion);

module.exports = router;