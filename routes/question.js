const router = require('express-promise-router')();
const questionController = require('../controllers/question');
const auth = require('../middleware/auth');

router.route('/add-question')
    .post(questionController.addQuestion);

router.route('/get-all-queries')
    .post(auth.auth, questionController.getAllQueries);

module.exports = router;
