const router = require('express-promise-router')();
const verifyOtpController = require('../controllers/verify-otp');

router.route('/')
    .post(verifyOtpController.verifyOtp);

module.exports = router;