const router = require('express-promise-router')();
const accountController = require('../controllers/account')

router.route('/')
    .post(accountController.login);

module.exports = router;