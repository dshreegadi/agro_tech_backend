const Sequelize = require('sequelize');
const sequelize = require('../config/database');
const Question = require('../models/question');
const Otp = require('../models/otp');

// class Answer extends Model{}
// Answer.init('answer', {
//     answerDesc: {
//         type: Sequelize.STRING
//     },
// })

class Answer extends Sequelize.Model {}
Answer.init({
    answerDesc: Sequelize.STRING
}, {
  underscored: false,
  sequelize,
  modelName: 'answer'
});

Answer.belongsTo(Otp); 


module.exports = Answer;