const Sequelize = require('sequelize');
const sequelize = require('../config/database');
const Otp = require('../models/otp');
const Answer = require('../models/answer');

// const Question = db.define('questions', {
//     questionDescription: {
//         type: Sequelize.STRING
//     },
// })

class Question extends Sequelize.Model { }
Question.init({
  questionDescription: Sequelize.STRING
}, {
  underscored: false,
  sequelize,
  modelName: 'question'
});

Question.belongsTo(Otp); 
Question.hasOne(Answer); 

module.exports = Question;