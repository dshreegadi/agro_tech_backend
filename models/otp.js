const Sequelize = require('sequelize');
const sequelize = require('../config/database');

class Otp extends Sequelize.Model{}

Otp.init({
    name: Sequelize.STRING,
    password: Sequelize.STRING,
    username: Sequelize.STRING,
    otp: Sequelize.INTEGER,
    regType: Sequelize.STRING,
    isOtpVerified: Sequelize.STRING
},  {
    underscored: false,
    sequelize,
    modelName: 'user'
})


module.exports = Otp;